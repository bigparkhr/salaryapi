package com.bg.salaryapi.entity;

import com.bg.salaryapi.model.Department;
import com.bg.salaryapi.model.DutyClass;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Salary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer employeeNumber;

    @Column(nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Department affiliatedDepartment;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private DutyClass dutyClass;

    @Column(nullable = false)
    private Double beforeTax;

    @Column(nullable = false)
    private Double noTax;
}
