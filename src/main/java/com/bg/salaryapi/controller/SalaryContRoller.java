package com.bg.salaryapi.controller;

import com.bg.salaryapi.model.SalaryRequest;
import com.bg.salaryapi.service.SalaryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/salary")
public class SalaryContRoller {
    private final SalaryService salaryService;

    @PostMapping("/new")
    public String setSalary(@RequestBody SalaryRequest request) {
        salaryService.setSalary(request);

        return "OK";
    }
}
