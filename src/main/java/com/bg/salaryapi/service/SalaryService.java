package com.bg.salaryapi.service;

import com.bg.salaryapi.entity.Salary;
import com.bg.salaryapi.model.SalaryRequest;
import com.bg.salaryapi.repository.SalaryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SalaryService {
    private final SalaryRepository salaryRepository;

    public void setSalary(SalaryRequest request) {
        Salary addData = new Salary();
        addData.setEmployeeNumber(request.getEmployeeNumber());
        addData.setName(request.getName());
        addData.setAffiliatedDepartment(request.getAffiliatedDepartment());
        addData.setDutyClass(request.getDutyClass());
        addData.setBeforeTax(request.getBeforeTax());
        addData.setNoTax(request.getNoTax());

        salaryRepository.save(addData);
    }
}
