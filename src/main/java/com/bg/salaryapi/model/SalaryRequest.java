package com.bg.salaryapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryRequest {
    private Integer employeeNumber;
    private String name;
    private Department affiliatedDepartment;
    private DutyClass dutyClass;
    private Double beforeTax;
    private Double noTax;
}
