package com.bg.salaryapi.repository;

import com.bg.salaryapi.entity.Salary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalaryRepository extends JpaRepository<Salary, Long> {
}
